<?php
namespace AppBundle\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;



class FreteTeste extends WebTestCase
{
	public static $container;
	public static $frete;

	public function setup() {
		//start the symfony kernel
		$kernel = static::createKernel();

		$kernel->boot();

		//get the DI container
		self::$container = $kernel->getContainer();

		//now we can instantiate our service (if you want a fresh one for
		//each test method, do this in setUp() instead
		self::$frete = self::$container->get('frete');


	}
    public function testIndex()
    {
        
        self::$frete->calcular(88340000, 1); die;

		/// escrever o teste aqui...
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }
}
