<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TransportadoraCep;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

/**
 * Transportadoracep controller.
 *
 * @Route("transportadoracep")
 */
class TransportadoraCepController extends Controller
{
    /**
     * Lists all transportadoraCep entities.
     *
     * @Route("/", name="transportadoracep_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $transportadoraCeps = $em->getRepository('AppBundle:TransportadoraCep')->findAll();

        return $this->render('transportadoracep/index.html.twig', array(
            'transportadoraCeps' => $transportadoraCeps,
        ));
    }

    /**
     * Creates a new transportadoraCep entity.
     *
     * @Route("/new", name="transportadoracep_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $transportadoraCep = new Transportadoracep();
        $form = $this->createForm('AppBundle\Form\TransportadoraCepType', $transportadoraCep);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {            
            // servi�o de gerenciamento de faixas de cep :)
            $serviceFaixaCep = $this->container->get('faixacep.validator');            
            $faixaConflitante = $serviceFaixaCep->isConflitante(
                    $transportadoraCep->getCepInicial(), 
                    $transportadoraCep->getCepFinal(),
                    $transportadoraCep->getTransportadora()
            );

            if ($faixaConflitante) {
                $form->addError(new FormError('
                    O CEP informado esta em conflito com outro 
                    ja cadastrado para esta transportadora.
                    Por favor, verifique.'
                ));
            }

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($transportadoraCep);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Faixa de CEP salva com sucesso!'
                );

                return $this->redirectToRoute('transportadoracep_show', array('id' => $transportadoraCep->getId()));
            }
        }            
        return $this->render('transportadoracep/new.html.twig', array(
            'transportadoraCep' => $transportadoraCep,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a transportadoraCep entity.
     *
     * @Route("/{id}", name="transportadoracep_show")
     * @Method("GET")
     */
    public function showAction(TransportadoraCep $transportadoraCep)
    {
        $deleteForm = $this->createDeleteForm($transportadoraCep);

        return $this->render('transportadoracep/show.html.twig', array(
            'transportadoraCep' => $transportadoraCep,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing transportadoraCep entity.
     *
     * @Route("/{id}/edit", name="transportadoracep_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TransportadoraCep $transportadoraCep)
    {
        $deleteForm = $this->createDeleteForm($transportadoraCep);
        $editForm = $this->createForm('AppBundle\Form\TransportadoraCepType', $transportadoraCep);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'success',
                'Faixa de CEP alterada com sucesso!'
            );
            return $this->redirectToRoute('transportadoracep_show', array('id' => $transportadoraCep->getId()));
        }

        return $this->render('transportadoracep/edit.html.twig', array(
            'transportadoraCep' => $transportadoraCep,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a transportadoraCep entity.
     *
     * @Route("/{id}", name="transportadoracep_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TransportadoraCep $transportadoraCep)
    {
        $form = $this->createDeleteForm($transportadoraCep);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($transportadoraCep);
            $em->flush();
            $this->addFlash(
                'success',
                'Faixa de CEP removida com sucesso!'
            );

        }
        return $this->redirectToRoute('transportadoracep_index');
    }

    /**
     * Creates a form to delete a transportadoraCep entity.
     *
     * @param TransportadoraCep $transportadoraCep The transportadoraCep entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TransportadoraCep $transportadoraCep)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('transportadoracep_delete', array('id' => $transportadoraCep->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
