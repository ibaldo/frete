<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Transportadora;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use AppBundle\Utils\Frete;

/**
 * Transportadora controller.
 *
 * @Route("transportadora")
 */
class TransportadoraController extends Controller
{
    /**
     * Lists all transportadora entities.
     *
     * @Route("/", name="transportadora_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $transportadoras = $em->getRepository('AppBundle:Transportadora')->findAll();
        return $this->render('transportadora/index.html.twig', array(
            'transportadoras' => $transportadoras,
        ));
    }

    /**
     * Creates a new transportadora entity.
     *
     * @Route("/new", name="transportadora_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $transportadora = new Transportadora();
        $form = $this->createForm('AppBundle\Form\TransportadoraType', $transportadora);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($transportadora);
            $em->flush();

            $this->addFlash(
                'success',
                'Transportadora salva com sucesso!'
            );

            return $this->redirectToRoute('transportadora_show', array('id' => $transportadora->getId()));
        }

        return $this->render('transportadora/new.html.twig', array(
            'transportadora' => $transportadora,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a transportadora entity.
     *
     * @Route("/{id}", name="transportadora_show")
     * @Method("GET")
     */
    public function showAction(Transportadora $transportadora)
    {
        $deleteForm = $this->createDeleteForm($transportadora);

        return $this->render('transportadora/show.html.twig', array(
            'transportadora' => $transportadora,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing transportadora entity.
     *
     * @Route("/{id}/edit", name="transportadora_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Transportadora $transportadora)
    {
        $deleteForm = $this->createDeleteForm($transportadora);
        $editForm = $this->createForm('AppBundle\Form\TransportadoraType', $transportadora);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Transportadora alterada com sucesso!'
            );
            return $this->redirectToRoute('transportadora_show', array('id' => $transportadora->getId()));
        }

        return $this->render('transportadora/edit.html.twig', array(
            'transportadora' => $transportadora,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a transportadora entity.
     *
     * @Route("/{id}", name="transportadora_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Transportadora $transportadora)
    {
        $form = $this->createDeleteForm($transportadora);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // remove as faixas de cep destra transportadora
            $transportadoraCepRepository = $em->getRepository('AppBundle:TransportadoraCep');
            
            $faixasCepTransportadora = $transportadoraCepRepository
                ->findAllByTransportadora($transportadora);

            if (count($faixasCepTransportadora > 0)) {
                foreach ($faixasCepTransportadora as $faixaCep) {
                    $em->remove($faixaCep);
                }
            }

            // remove transportadora.
            $em = $this->getDoctrine()->getManager();
            $em->remove($transportadora);
            $em->flush();

            $this->addFlash(
                'success',
                'Transportadora removida com sucesso.'
            );

        }

        return $this->redirectToRoute('transportadora_index');
    }
    
    /**
     * Changes the status of transportadora entity.
     *
     * @Route("/changestatus/{id}", name="transportadora_changestatus")
     * @Method("GET")
     */
    public function changestatusAction(Request $request, Transportadora $transportadora)
    {
            if ($transportadora->getStatus() == 1) {
                $transportadora->setStatus(Transportadora::STATUS_INATIVO);
            } else {
                $transportadora->setStatus(Transportadora::STATUS_ATIVO);
            }            
            $em = $this->getDoctrine()->getManager();
            $em->persist($transportadora);
            $em->flush();
            $this->addFlash(
                'success',
                'O status da transportadora foi alterado com sucesso.'
            );

            return $this->redirectToRoute('transportadora_index');
    }

    /**
     * Creates a form to delete a transportadora entity.
     *
     * @param Transportadora $transportadora The transportadora entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Transportadora $transportadora)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('transportadora_delete', array('id' => $transportadora->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
