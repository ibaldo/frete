<?php
namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\TransportadoraCep;
use AppBundle\Entity\Transportadora;

/**
 * Serviço FaixaCep
 *
 * Usado para validar faixas de cep concomitantes
 * 
 */
class FaixaCep
{

    /**
     * Gerenciador de Entidades da classe (EntityManager)
     *
     * @return Doctrine\ORM\EntityManager;
     */    
    protected $em;


    /**
     * Construtor... 
     *
     * Seta o gerenciador de entidades da classe 
     *
     * @param Doctrine\ORM\EntityManager $entitymanager
     * @return $this
     */
    public function __construct(EntityManager $entityManager){
        $this->em = $entityManager;
    }


    /**
     * Valida faixas de cep concomitantes por transportadora
     * 
     * @todo Verificar se o metodo  findAllByTransportadora nao
     * pode ser substituído pleo findBy com um criteria de transportadora
     * 
     * @param integer $cepInicial
     * @param integer $cepFinal
     *
     *
     * @return boolean true se faixa de cep não são concomitantes
     */
    public function isConflitante($cepInicial, $cepFinal, $transportadora) 
    {              
        $repository = $this->em->getRepository('AppBundle:TransportadoraCep');
        $faixasCep  = $repository->findAllByTransportadora($transportadora);
        if (count(0 < $faixasCep)) {
            foreach ($faixasCep as $faixa) {
                $conflitoFaixaInicial = (bool)($cepInicial >= $faixa->getCepInicial() && $cepInicial <= $faixa->getCepFinal()); // fugindo de um if gigante ...
                $conflitoFaixaFinal   = (bool)($cepFinal >= $faixa->getCepInicial() && $cepFinal <= $faixa->getCepFinal()); // fugindo de um if gigante ...
                
                if($conflitoFaixaInicial || $conflitoFaixaFinal) {
                    return true;
                }                             
            }
        }
        return false;                                           
    }
}