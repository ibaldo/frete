<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Frete;
use AppBundle\Entity\Transportadora;
use AppBundle\Entity\TransportadoraCep;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;


/**
 * Frete Conroller
 * Gerencia as requisi��es respons�veis pelo c�lculo de frete
 *
 * @Route("/frete")
 */
class FreteController extends Controller
{
    /**
     * A��o que recebe os par�metros de cep e peso passados
     * pelo formul�rio de frete e encaminha a requisi��o
     * para o servi�o de c�lculo.
     *
     *
     * @param Request $request
     * @Route("/calcular", name="frete_calcular")
     */
    public function calcularAction(Request $request)
    {
    	$frete = new Frete();   
        $form = $this->createFormBuilder($frete)->getForm();

        if ($request->isMethod('POST')) {

            $frete->setCep($request->get('cep'));
            $frete->setPeso($request->get('peso'));
            
            $validator = $this->get('validator');            
            $errors = $validator->validate($frete);

            if (count($errors) == 0) {    
                $serviceFrete = $this->container->get('frete');
                $resultados   = $serviceFrete->calcular($frete->getCep(), $frete->getPeso());
                if (empty($resultados)) {
                    /*  @todo procurar uma clase de erro mais adequada...  */
                    $errors->add(new ConstraintViolation('O CEP informado nao foi encontrado em nenhuma faixa de CEP disponivel.', false, array(), null, false, false));
                }
            } 
                                        
		}
                 
        return $this->render('AppBundle:Frete:calcular.html.twig', array(
            'frete' => null,
            'form' => $form->createView(),
            'cep'  => $frete->getCep(),
            'peso' => $frete->getPeso(),
            'resultados' => empty($resultados) ? null : $resultados,
            'errors' => empty($errors) ? null : $errors
        ));

    }
}





















