<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Transportadora
 *
 * @ORM\Table(name="transportadora")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TransportadoraRepository")
 * @UniqueEntity(
 *     fields={"cnpj"},
 *     message="Foi encontrada  uma outra Transportadora cadastrada com este CNPJ. Por favor, verifique."
 * )
 */
class Transportadora
{

    /**
     * Determina se a Transportadora est� ativa
     */    
    const STATUS_INATIVO = 0;

    /**
     * Determina se a Transportadora est� inativa
     */        
    const STATUS_ATIVO   = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="cnpj", type="string", unique=true)
     *
     */
    private $cnpj;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Transportadora
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Transportadora
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
   /**
     * Set cnpj
     *
     * @param string $cnpj
     * @return Transportadora
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Get cnpj
     *
     * @return string 
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @ORM\OneToMany(targetEntity="TransportadoraCep", mappedBy="ceps")
     */
    protected $ceps;

    public function __construct()
    {
        $this->ceps = new ArrayCollection();
    }

    /**
     * Add ceps
     *
     * @param \AppBundle\Entity\TransportadoraCep $ceps
     * @return Transportadora
     */
    public function addCep(\AppBundle\Entity\TransportadoraCep $ceps)
    {
        $this->ceps[] = $ceps;

        return $this;
    }

    /**
     * Remove ceps
     *
     * @param \AppBundle\Entity\TransportadoraCep $ceps
     */
    public function removeCep(\AppBundle\Entity\TransportadoraCep $ceps)
    {
        $this->ceps->removeElement($ceps);
    }

    /**
     * Get ceps
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCeps()
    {
        return $this->ceps;
    }

    
    /**
     * Usado para exibir o nome de um objeto Transportadora 
     *
     * @return string $nome
     */    
    public function __toString() {
        return $this->nome;
    }    
}
