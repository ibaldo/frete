<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Frete
 * 
 * Representa um objeto do tipo frete utilizado no frontend para que o usu�rio
 * informe dados para calculo do frete.
 * 
 * Esta entidade n�o persiste no banco de dados. Ela existe apenas para que a 
 * estrutura utilizada no frontend tamb�m seja orientada � objetos.
 *
 * @ORM\Table(name="frete")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FreteRepository")
 */
class Frete
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=10)    
     * @Assert\NotBlank(message = "Por favor, preencha o CEP corretamente.")     
     */
    public $cep;

    /**
     * @var float
     *
     * @ORM\Column(name="peso", type="float")
     * @Assert\NotBlank(message = "Por favor, informe o peso para calculo do frete.")          
     */
    private $peso;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cep
     *
     * @param string $cep
     * @return Frete
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string 
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set peso
     *
     * @param float $peso
     * @return Frete
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return float 
     */
    public function getPeso()
    {
        return $this->peso;
    }

}
