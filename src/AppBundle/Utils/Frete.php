<?php
// src/AppBundle/Utils/Frete.php
namespace AppBundle\Utils;

class Frete
{
    public function slugify($string)
    {
        return preg_replace(
            '/[^a-z0-9]/', '-', strtolower(trim(strip_tags($string)))
        );
    }

  
}