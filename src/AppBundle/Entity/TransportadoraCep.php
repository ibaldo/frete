<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * TransportadoraCep
 *
 *
 * Representa a entidade de uma Transportadora_Cep no banco de dados
 *
 *
 * @todo Refatorar esta classe e chamá-la de FaixasCep pois a representação
 * mais relevante dela são as FAIXAS de CEP e não CEP em si. Transportadora
 * também é apenas uma relação de chave estrangeira, não sendo o mais adequado 
 * conter a palavra Transportadora no nome.
 * 
 *
 * @ORM\Table(name="transportadora_cep")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TransportadoraCepRepository")
 */
class TransportadoraCep
{

    /**
     * @ORM\ManyToOne(targetEntity="Transportadora", inversedBy="ceps")
     * @ORM\JoinColumn(name="transportadora_id", referencedColumnName="id")
     */
    protected $transportadora;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cepInicial", type="integer")
     */
    private $cepInicial;

    /**
     * @var int
     *
     * @ORM\Column(name="cepFinal", type="integer")
     */
    private $cepFinal;

    /**
     * @var float
     *
     * @ORM\Column(name="pesoInicial", type="float")
     */
    private $pesoInicial;

    /**
     * @var float
     *
     * @ORM\Column(name="pesoFinal", type="float")
     */
    private $pesoFinal;

    /**
     * @var float
     *
     * @ORM\Column(name="valorKg", type="float")
     */
    private $valorKg;

    /**
     * @var float
     *
     * @ORM\Column(name="valorKgAdicional", type="float")
     */
    private $valorKgAdicional;

    /**
     * @var int
     *
     * @ORM\Column(name="prazo", type="integer")
     */
    private $prazo;

    /**
     * @var int
     *
     * @ORM\Column(name="prazoAdicional", type="integer")
     */
    private $prazoAdicional;

    /**
     * @var int
     *
     * @ORM\Column(name="razaoPesoAdicional", type="integer")
     */
    private $razaoPesoAdicional;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cepInicial
     *
     * @param integer $cepInicial
     * @return TransportadoraCep
     */
    public function setCepInicial($cepInicial)
    {
        $this->cepInicial = $cepInicial;

        return $this;
    }

    /**
     * Get cepInicial
     *
     * @return integer 
     */
    public function getCepInicial()
    {
        return $this->cepInicial;
    }

    /**
     * Set cepFinal
     *
     * @param integer $cepFinal
     * @return TransportadoraCep
     */
    public function setCepFinal($cepFinal)
    {
        $this->cepFinal = $cepFinal;

        return $this;
    }

    /**
     * Get cepFinal
     *
     * @return integer 
     */
    public function getCepFinal()
    {
        return $this->cepFinal;
    }

    /**
     * Set pesoInicial
     *
     * @param float $pesoInicial
     * @return TransportadoraCep
     */
    public function setPesoInicial($pesoInicial)
    {
        $this->pesoInicial = $pesoInicial;

        return $this;
    }

    /**
     * Get pesoInicial
     *
     * @return float 
     */
    public function getPesoInicial()
    {
        return $this->pesoInicial;
    }

    /**
     * Set pesoFinal
     *
     * @param float $pesoFinal
     * @return TransportadoraCep
     */
    public function setPesoFinal($pesoFinal)
    {
        $this->pesoFinal = $pesoFinal;

        return $this;
    }

    /**
     * Get pesoFinal
     *
     * @return float 
     */
    public function getPesoFinal()
    {
        return $this->pesoFinal;
    }

    /**
     * Set valorKg
     *
     * @param float $valorKg
     * @return TransportadoraCep
     */
    public function setValorKg($valorKg)
    {
        $this->valorKg = $valorKg;

        return $this;
    }

    /**
     * Get valorKg
     *
     * @return float 
     */
    public function getValorKg()
    {
        return $this->valorKg;
    } 

    /**
     * Set valorKgAdicional
     *
     * @param float $valorKgAdicional
     * @return TransportadoraCep
     */
    public function setValorKgAdicional($valorKgAdicional)
    {
        $this->valorKgAdicional = $valorKgAdicional;

        return $this;
    }

    /**
     * Get valorKgAdicional
     *
     * @return float 
     */
    public function getValorKgAdicional()
    {
        return $this->valorKgAdicional;
    }

    /**
     * Set prazo
     *
     * @param integer $prazo
     * @return TransportadoraCep
     */
    public function setPrazo($prazo)
    {
        $this->prazo = $prazo;

        return $this;
    }

    /**
     * Get prazo
     *
     * @return integer 
     */
    public function getPrazo()
    {
        return $this->prazo;
    }


    /**
     * Set prazo adicional
     *
     * @param integer $prazoAdicional
     * @return TransportadoraCep
     */
    public function setPrazoAdicional($prazoAdicional)
    {
        $this->prazoAdicional = $prazoAdicional;

        return $this;
    }

    /**
     * Get prazoAdicional
     *
     * @return integer 
     */
    public function getPrazoAdicional()
    {
        return $this->prazoAdicional;
    }


    /**
     * Set razão de Peso adicional
     *
     * @param integer $razaoPesoAdicional
     * @return TransportadoraCep
     */
    public function setRazaoPesoAdicional($razaoPesoAdicional)
    {
        $this->razaoPesoAdicional = $razaoPesoAdicional;

        return $this;
    }

    /**
     * Get razão de Peso Adicional
     *
     * @return integer 
     */
    public function getRazaoPesoAdicional()
    {
        return $this->razaoPesoAdicional;
    }


    public function setTransportadora(\AppBundle\Entity\Transportadora $transportadora = null)
    {
        $this->transportadora = $transportadora;

        return $this;
    }

    /**
     * Get transportadora
     *
     * @return \AppBundle\Entity\Transportadora 
     */
    public function getTransportadora()
    {
        return $this->transportadora;
    }

    /**
     * Calcula o valor final de um frete
     *
     * @param float $peso
     * @return float 
     */  
    public function getValorFinal($peso)
    {
                
        /* @todo validar peso como float, casas decimais, etc */   
        if ($peso >= $this->pesoInicial && $peso <= $this->pesoFinal) {
            return $this->valorKg * $peso;
        }
        
        $valorNormal = $this->pesoFinal * $this->valorKg;
        $sobrepeso = $peso - $this->pesoFinal;

        return ($sobrepeso * $this->valorKgAdicional) + $valorNormal;
    }

    /**
     * Calcula o prazo final de um frete
     *     
     * @param float $peso
     * @return float 
     */  
    public function getPrazoFinal($peso)
    {
        /* @todo validar peso como float, casas decimais, etc */   
        if ($peso <= $this->pesoFinal) {
            return $this->prazo;
        }
        $sobrepeso = $peso - $this->pesoFinal;
        $razaoPeso = ceil($sobrepeso / $this->razaoPesoAdicional);
        return $this->prazo + ($razaoPeso * $this->prazoAdicional);
    }


    
}
