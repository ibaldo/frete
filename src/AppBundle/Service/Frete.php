<?php
namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\TransportadoraCep;
use AppBundle\Entity\Transportadora;
use AppBundle\Repository\TransportadoraCepRepository;

/**
 * Serviço Frete
 *
 * Requisitado para efeutar cálculos de frete a partir de CEP e PESO.
 * 
 */
class Frete
{

    /**
     * Gerenciador de Entidades da classe (EntityManager)
     *
     * @return Doctrine\ORM\EntityManager;
     */    
    protected $transportadoraCepRepository;


    /**
     * Construtor... 
     *
     * Seta o gerenciador de entidades da classe 
     *
     * @param Doctrine\ORM\EntityManager $entitymanager
     * @return $this
     */
    public function __construct(TransportadoraCepRepository $transportadoraCepRepository)
    {
        $this->transportadoraCepRepository = $transportadoraCepRepository;
    }


    /**
     * Calcula e ordena as faixas de CEP válidas pelos parâmetros
     * de cep e peso informados pelo invocador do serviço
     *
     * @param integer $cep
     * @param  floar $peso
     *
     * @return array de faixas ordenadas
     */
    public function calcular($cep, $peso) 
    {              
        $faixasCepValidas = $this->transportadoraCepRepository->retornaFaixasValidas($cep);
        return $this->retornaFaixasOrdenadas($faixasCepValidas, $peso);                 
    }


    /**
     * Ordena e retorna faixas de cep ordenadas 
     * pelo menor valor e  prazo
     *
     *
     * @param  array $faixasCepValidas
     * @param  floar $peso
     *
     * @return array de faixas ordenadas
     */    
    private function retornaFaixasOrdenadas($faixasCepValidas, $peso) 
    {
        if(empty($faixasCepValidas)) {
            return null;
        }           

        usort(
            $faixasCepValidas, 
            function (TransportadoraCep $primeiraFaixa, TransportadoraCep $segundaFaixa) use ($peso)  {
                $primeiroValor = $primeiraFaixa->getValorFinal($peso);
                $segundoValor = $segundaFaixa->getValorFinal($peso);

                if ($primeiroValor == $segundoValor) {
                    $primeiroPrazo = $primeiraFaixa->getPrazoFinal($peso);
                    $segundoPrazo = $segundaFaixa->getPrazoFinal($peso);
                    if ($primeiroPrazo == $segundoPrazo) {
                        return 0;
                    }

                    return $primeiroPrazo < $segundaFaixa->getPrazoFinal($peso) ? -1  : 1;
                }

                return $primeiraFaixa->getValorFinal($peso) < $segundaFaixa->getValorFinal($peso) ? - 1 : 1;
        });
        
        return $faixasCepValidas;
    }   

    /**
     * @todo para testes unitários... de teste. :P
     */
    public function calculate() 
    {
        return 2;
    }
}